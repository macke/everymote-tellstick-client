var exec = require('child_process').exec,
	config = require('./config');
var _things = [];
	
var createThing = function(thingData){
	return { 
		"name":thingData[1],
		"id":thingData[0],
		"position": config.getPosition(),
		"quickAction":{"button":"switch"},
		"functions":[{"button":"On"}, {"button":"Off"}]
	};	
};

var parsThing = function(thingString){
	var thingData = thingString.split('\t');	
	return 	createThing(thingData);
};

exports.turnOn = function(thing){ exec("tdtool --on " + thing.id, function (error, stdout, stderr) {
	  console.log('stdout: ' + stdout);
	  console.log('stderr: ' + stderr);
	  if (error !== null) {
	    console.log('exec error: ' + error);
	  }
	});
};


exports.turnOff = function(thing){ exec("tdtool --off " + thing.id, function (error, stdout, stderr) {
	  console.log('stdout: ' + stdout);
	  console.log('stderr: ' + stderr);
	  if (error !== null) {
	    console.log('exec error: ' + error);
	  }
	});
};

exports.getThings = function(callback){
		exec("tdtool -l", function (error, stdout, stderr) {
		var thingStrings = stdout.split('\n');
		for(var i = 1; i < thingStrings.length - 1; i++){
		    _things.push(parsThing(thingStrings[i]));
		}	
	  console.log(_things);
	  console.log('stderr: ' + stderr);
	  if (error !== null) {
	    console.log('exec error: ' + error);
	  }

	  callback(_things);
	});	
};	
