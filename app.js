var io = require('socket.io-client'),
   tellstick = require('./tellstick');

var port = '1337',
	server =  'm.everymote.com';


var connectLamp = function(lamp){
	console.log(lamp);
	var socket = io.connect('http://' + server + ':' + port + '/thing',{"force new connection":true});
	
	var lampOn = false;
	socket.on('connect', function () {
		console.log('connected');
		socket.emit('setup', lamp);
	}).on('doAction', function (action) {
		console.log(action);
		if(action == "On"){
			tellstick.turnOn(lamp);
			lampOn = true;
		}else if(action== "Off"){
			tellstick.turnOff(lamp);
			lampOn = false;
		}else{
			if(lampOn){
				tellstick.turnOff(lamp);

			}else{
				tellstick.turnOn(lamp);
			}
			lampOn = !lampOn;
		}
	}).on('connect_failed', function () {
		console.log('error:' + socket );
	}).on('disconnect', function () {
		console.log('disconnected');
	});
};

var connectThings = function (things){
	things.map(connectLamp);
};	

tellstick.getThings(connectThings);


process.on('uncaughtException', function(err){
	console.log('Something bad happened: ' + err);
	process.exit(0);
});
